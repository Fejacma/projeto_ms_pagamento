package br.com.itau.pagamento.dtos;

public class PagamentoDTOEntradaPost {

    private long cartaoId;
    private String descricao;
    private double valor;
    private String cartaoNumero;

    public PagamentoDTOEntradaPost() { }

    public String getCartaoNumero() {
        return cartaoNumero;
    }

    public void setCartaoNumero(String cartaoNumero) {
        this.cartaoNumero = cartaoNumero;
    }

    public long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public double getValor() { return valor; }

    public void setValor(double valor) { this.valor = valor; }


}

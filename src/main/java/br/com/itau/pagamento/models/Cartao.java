package br.com.itau.pagamento.models;

public class Cartao {

    private long id;

    private String cartaoNumero;

    private boolean ativo;

    public Cartao() { }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getCartaoNumero() { return cartaoNumero; }

    public void setCartaoNumero(String cartaoNumero) { this.cartaoNumero = cartaoNumero; }

    public boolean isAtivo() { return ativo; }

    public void setAtivo(boolean ativo) { this.ativo = ativo; }

}

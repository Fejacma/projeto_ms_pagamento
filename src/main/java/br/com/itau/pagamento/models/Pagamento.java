package br.com.itau.pagamento.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String cartaoNumero;

    private long cartaoId;

    private String descricao;

    private double valor;

    public Pagamento() { }

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public String getCartaoNumero() {
        return cartaoNumero;
    }

    public void setCartaoNumero(String cartaoNumero) {
        this.cartaoNumero = cartaoNumero;
    }

    public long getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(long cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() { return descricao; }

    public void setDescricao(String descricao) { this.descricao = descricao; }

    public double getValor() { return valor; }

    public void setValor(double valor) { this.valor = valor; }

}

package br.com.itau.pagamento.controllers;

import br.com.itau.pagamento.dtos.PagamentoDTOEntradaPost;
import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pagamento")
public class PagamentoContoller {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    public ResponseEntity<Pagamento> postPagamento(@RequestBody PagamentoDTOEntradaPost pagamentoDTOEntradaPost) {
        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoNumero(pagamentoDTOEntradaPost.getCartaoNumero());
        pagamento.setDescricao(pagamentoDTOEntradaPost.getDescricao());
        pagamento.setValor(pagamentoDTOEntradaPost.getValor());

        Pagamento pagamentoObjeto = pagamentoService.salvarPagamento(pagamento);

        return ResponseEntity.status(201).body(pagamentoObjeto);
    }

    @GetMapping("/{cartao_id}")
    public Iterable<Pagamento> getPagamentosByCartaoId(@PathVariable(name = "cartao_id") Long cartaoId) {
        Iterable<Pagamento> pagamentos = pagamentoService.consultarPagamentosPorCartaoId(cartaoId);
        return pagamentos;
    }

}

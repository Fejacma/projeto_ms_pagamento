package br.com.itau.pagamento.services;

import br.com.itau.pagamento.clients.CartaoClient;
import br.com.itau.pagamento.models.Cartao;
import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    public Pagamento salvarPagamento(Pagamento pagamento) {

        Cartao cartaoObjeto = cartaoClient.consultarPorNumero(pagamento.getCartaoNumero());
        pagamento.setCartaoId(cartaoObjeto.getId());
        boolean ativo = cartaoObjeto.isAtivo();

        if (ativo) {
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;
        }

        throw new RuntimeException("Seu cartão não está ativo. Favor ativar o Cartão antes de realizar sua compra.");
    }

    //        boolean ativo = cartaoObjeto.isAtivo();
//
//        pagamento.getCartaoId().setAtivo(ativo);
//
////        if (ativo) {
//            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
//            return pagamentoObjeto;
////        }
//
//        throw new RuntimeException("Seu cartão não está ativo. Favor ativar o Cartão antes de realizar sua compra.");

    public Iterable<Pagamento> consultarPagamentosPorCartaoId(long cartaoId) {
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(cartaoId);
        return pagamentos;
    }

}

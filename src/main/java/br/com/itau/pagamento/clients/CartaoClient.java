package br.com.itau.pagamento.clients;


import br.com.itau.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Cartao")
public interface CartaoClient {

    @GetMapping("/cartao/{numero}")
    public Cartao consultarPorNumero(@PathVariable String numero);

}
